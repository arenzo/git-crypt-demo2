# Git-Crypt Demo
This is a demo repo for using Git-Crypt for in-code secrets encryption.

## Installing Git Crypt
1) Git Crypt should first be installed:

**Linux:**  `sudo apt install -y openssl git git-crypt`

**Windows:** [Windows Installation](https://github.com/AGWA/git-crypt/blob/master/INSTALL.md)

**OSX:** `brew install git-crypt`


## Using Git Crypt
1) **Initialize Git-Crypt:** Initialization happens when you initially clone the repository, like so:

```
git clone repo-url
git-crypt init
```

2) **Unlock the Repo:** Once you have obtained the key, you can unlock the repo to do work:

```
git-crypt unlock /path/to/key.asc
```

3) From this moment forward, the encrypted files will be fully readable by you.  Files will be re-encrypted seamlessly during commits and pushes back to the remote repository.
